# NLP Virtual Agent Framework

Studio integrates with Dialogflow to create powerful voice and text natural language agents. Get started fast with the NLP VA framework task library entry.

This repository provides support resources for that task library entry.

`NLP_VA_Framework.zip` is a small demonstration Google Dialogflow agent that integrates and provides standard The Dialogflow agent framework.
 
 * Provides common intent handling for conversational AI such as fallback intents, escalation to live agent and caller completion disconnection
 * Provides two sample intents about studio access, partner portal, and partner portal access.
 * Shows how to use follow-up intents, which allows the one bot to give two different answers to the question "how do I get access"
 * Illustrates using contexts as one mechanism for triggering common fulfilments for many intents - like transfer to live agent, send links, wait for caller
 * Illustrates using using intent specific fulfilments on intent completion in Studio
 
## Sample Conversation
| Person | Virtual Agent  |
| ------------- | ------- |
| `caller arrives ` | This framework..... |
| How do I get access? | Your account invite..... was there anything else? |
| Can you hold for one moment please? | Sure I'll be right here when you need me |
| ... | |
| ... | |
| Do you have a portal for partners? | The inference partner portal..... I've sent you a link now... was there anything else? |
| How do I get access? | You need to use your work email.... intent specific fulfilment outside dialogflow .... |
| That's all thanks, bye | Glad to be of assistance.... |
|`caller disconnects` | `VA does after call work` |

# Set up
## Create Google Dialogflow Agent
1. Create a Google account first if you do not have one at https://accounts.google.com
2. Create an account on [dialogflow console](https://console.dialogflow.com). Allow Dialogflow access to your Google Account
3. Create an agent and give it a name.
4. Restore the template agent by choosing `Settings` (the cog in the top left next to the agent name) | `Export and Import` | `Restore from ZIP` then select NLP_VA_Framework.zip and enter RESTORE into the confirmation box in order to be able to click `RESTORE`. Choose `DONE`

## Allow Studio access in Google Cloud IAM
Follow the steps below or see steps 4 - 7 of our [docs on open form types](https://docs.inferencesolutions.com/tutorials/#creating-an-open-form-type)

1. Under the General Settings now shown, take a copy of the name of the *Project ID* 
2. Follow the project ID link to https://console.cloud.google.com/.  Agree to the terms of service if this is the first time using Google Cloud Platform
2. Choose `IAM & Admin` | `IAM`
3. Grant the [Studio identities for each region](https://docs.inferencesolutions.com/reference/#open-form-types) access to the role *Dialogflow API Client*

## Studio

1. Create a new task from the task library, selecting the NLP VA Framework
2. Edit the Open Form Type automatically created by the task library in `Global` | `Open Form Types` and update the project ID to match your new Dialogflow agent.
3. Add a pause SSML entry to the SSML Snippets datastore via `Global` | `Datastores` | `Actions` | `Insert Data`
    * *Title*: `Pause 10 seconds`
    * *SSML*: `<break time="10s"/>`
3. Map a number to your task or call it with WebRTC.